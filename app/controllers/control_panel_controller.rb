class ControlPanelController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "password"
 
  def index
    @timeline = Timeline.all
    @tline = Timeline.order(hover_no: :desc)
  end

end
