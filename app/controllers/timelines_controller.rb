class TimelinesController < ApplicationController

  def index

  end

  def create
    @timeline = Timeline.new(timeline_params)
    

    respond_to do |format|
      if @timeline.save
        render json: { status: :ok, timeline: @timeline}
      else        
        format.json { render json: @timeline.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end 

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def timeline_params
      params.require(:timeline).permit(:name, :hover_no)
    end
  
end
