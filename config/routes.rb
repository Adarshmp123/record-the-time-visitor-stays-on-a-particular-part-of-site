Rails.application.routes.draw do
  get 'admin' => 'control_panel#index', :as => :admin

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
   root 'timelines#index'
  post '/visitors' => 'timelines#create'
end
